with Ada.Strings.Maps;
with Ada.Text_IO; use Ada.Text_IO;
with Automata_Scan;

procedure Main is
   pragma SPARK_Mode;

   type State_Type is
     (
      Start,
      Segment_Head,
      Maybe_Complement,
      Segment_Body,
      Maybe_Range,
      One_Slash,
      After_Dash
     );

   type Action_Type is
     (
      Nothing,
      Error,
      End_Of_Parsing,
      Initialize,
      Save_Char,
      Add_Saved,
      Add_Current_Char,
      Add_Saved_And_Save,
      Add_Range,
      Prohibited_Set,
      Complement_Set,
      Close_Set
     );

   package Password_Style_Scan is
     new Automata_Scan (State_Type  => State_Type,
                        Action_Type => Action_Type,
                        Start       => Start,
                        Error       => Error);
   use Password_Style_Scan;

   package Saving_Buffer is
      Buffer : Character;
      Full   : Boolean := False;

      procedure Save (C : Character)
        with
          Pre => not Full, Post => Full;

      pragma Warnings (Off, "postcondition does not mention function result");
      function Get return Character
        with
          Pre => Full, Post => not Full;
   end Saving_Buffer;

   package body Saving_Buffer is
      procedure Save (C : Character) is
      begin
         Buffer := C;
         Full := True;
      end Save;

      function Get return Character
      is
      begin
         Full := False;
         return Buffer;
      end Get;
   end Saving_Buffer;

   Scanner : Automata_Type := Create;
begin

   Add_Transition (Automata => Scanner,
                   From     => Start,
                   Input    => '/',
                   To       => Segment_Head,
                   Action   => Initialize);

   Add_Transition (Automata => Scanner,
                   From     => Segment_Head,
                   Input    => '!',
                   To       => Maybe_Complement,
                   Action   => Prohibited_Set);

   Add_Transition (Automata => Scanner,
                   From     => Segment_Head,
                   Input    => '?',
                   To       => Segment_Body,
                   Action   => Complement_Set);

   Add_Transition (Automata => Scanner,
                   From     => Maybe_Complement,
                   Input    => '?',
                   To       => Segment_Body,
                   Action   => Complement_Set);


   Add_Transition (Automata => Scanner,
                   From     => Segment_Body,
                   Input    => '-',
                   To       => Segment_Body,
                   Action   => Add_Current_Char);


   Add_Transition (Automata => Scanner,
                   From     => Segment_Body,
                   Input    => '/',
                   To       => One_Slash,
                   Action   => Nothing);


   Add_Transition (Automata => Scanner,
                   From     => One_Slash,
                   Input    => '/',
                   To       => Maybe_Range,
                   Action   => Save_Char);


   Add_Transition (Automata => Scanner,
                   From     => Maybe_Range,
                   Input    => '-',
                   To       => After_Dash,
                   Action   => Nothing);

   Add_Transition (Automata => Scanner,
                   From     => Maybe_Range,
                   Input    => '/',
                   To       => One_Slash,
                   Action   => Add_Saved);


   On_End_Of_Input (Automata    => Scanner,
                    Final_State => One_Slash,
                    Action      => End_Of_Parsing);

   Default_Transition (Automata => Scanner,
                       From     => Maybe_Range,
                       To       => Maybe_Range,
                       Action   => Add_Saved_And_Save);

   Default_Transition (Automata => Scanner,
                       From     => After_Dash,
                       To       => Segment_Body,
                       Action   => Add_Range);

   Default_Transition (Automata => Scanner,
                       From     => One_Slash,
                       To       => Segment_Head,
                       Action   => Close_Set,
                       Ungetc => True);

   Default_Transition (Automata => Scanner,
                       From     => Segment_Body,
                       To       => Maybe_Range,
                       Action   => Save_Char);

   Default_Transition (Automata => Scanner,
                       From     => Segment_Head,
                       To       => Maybe_Range,
                       Action   => Save_Char);

   Default_Transition (Automata => Scanner,
                       From     => Maybe_Complement,
                       To       => Maybe_Range,
                       Action   => Save_Char);

   Reset (Automata => Scanner,
          Input    => "/a-z/A-Z/-$//%()/0-9/!?a-z0-9/");

   declare
      use Ada.Strings.Maps;

      Char         : Character;
      Action       : Action_Type;
      Result       : Character_Set;
      Prohibited   : Boolean;
      Complemented : Boolean;

      procedure Add_Single_Char (C : Character)
      is
      begin
         Result := Result or To_Set (C);
      end Add_Single_Char;

      procedure Add_Range (From, To : Character)
      is
      begin
         Result := Result or To_Set (Character_Range'(From, To));
      end Add_Range;

      procedure Initialize_Current_Set is
      begin
         Result := Null_Set;
         Prohibited := False;
         Complemented := False;
      end Initialize_Current_Set;

      procedure Close_Current_Set is
      begin
         if Prohibited then
            Put ("NO ");
         end if;

         if Complemented then
            Put ("^ ");
         end if;

         Put_Line (To_Sequence (Result));
         Initialize_Current_Set;
      end Close_Current_Set;

      procedure Mark_As_Prohibited is
      begin
         Prohibited := True;
      end Mark_As_Prohibited;

      procedure Mark_As_Complemented is
      begin
         Complemented := True;
      end Mark_As_Complemented;
   begin
      loop
         Next (Automata => Scanner,
               Action   => Action,
               Char     => Char);

         case Action is
            when Nothing =>
               null;

            when Error =>
               raise Constraint_Error;

            when Initialize =>
               Initialize_Current_Set;

            when Prohibited_Set =>
               Mark_As_Prohibited;

            when Complement_Set =>
               Mark_As_Complemented;

            when Save_Char =>
               Saving_Buffer.Save (Char);

            when Add_Saved =>
               Add_Single_Char (Saving_Buffer.Get);

            when Add_Current_Char =>
               Add_Single_Char (Char);

            when Add_Saved_And_Save =>
               Add_Single_Char (Saving_Buffer.Get);
               Saving_Buffer.Save (Char);

            when Add_Range =>
               Add_Range (Saving_Buffer.Get, Char);

            when Close_Set =>
               Close_Current_Set;

            when End_Of_Parsing =>
               Close_Current_Set;
               exit;
         end case;
      end loop;
   end;

end Main;
