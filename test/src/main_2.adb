with Ada.Strings.Maps;
with Ada.Text_IO; use Ada.Text_IO;

with Password_Style_Parsers;

procedure Main_2 is
   pragma SPARK_Mode;
begin
   declare
      use Password_Style_Parsers;

      Sets : Password_Style_Descriptor := Parse ("/a-z/A-Z/-$//%()/0-9/!.*/!@=/");
   begin
      if Is_Valid (Sets) then
         Put_Line ("OK");
      else
         Put_Line ("NO");
      end if;

      Put_Line (Image (Sets));
   end;
end Main_2;
